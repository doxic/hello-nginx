# Hello-Nginx

Simple repository for demo deployments to AWS ECR / ECS with Gitlab CI/CD.

## Prerequisites
Installation of awscli is done with `pip` using Python 3 and venv.

Install git, python3 and pip3
```shell
sudo apt update && apt install git python3 python3-pip python3-venv
```

Clone this repository locally
```shell
$ git clone https://gitlab.com/doxic/hello-nginx.git
$ cd hello-nginx
```

Test for existing awscli and remove it if existent
```shell
$ aws --version
$ pip3 uninstall awscli
```

Create a virtual environment
```shell
$ python3 -m venv .venv
```

Activate virtual environment.
```shell
$ source .venv/bin/activate
```

Upgrade pip and install project dependencies
```shell
$ pip install --upgrade pip
$ pip install -r requirements.txt
```

## AWS IAM
[Do not use](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_root-user.html) AWS Account Root for ECR tasks. Create a poweruser for administrative tasks and a restricted user for programmatic access

Environment variables
```shell
$ export AWS_ACCESS_KEY_ID=<Your "Root/FullAccess Access key ID">
$ export AWS_SECRET_ACCESS_KEY=<Your "Root/FullAccess Secret access key">

$ export AWS_IAM_USER=doxic
```


Environment variables
```shell
$ export AWS_ACCESS_KEY_ID=<Your "User Access key ID">
$ export AWS_SECRET_ACCESS_KEY=<Your "User Secret access key">

$ export AWS_IAM_USER=ecr-remote-pusher
```

Use the create-user command to create restricted IAM user `ecr-remote-pusher` for programmatic access.

```shell
$ aws iam create-user --user-name $AWS_IAM_USER
```

Required managed policies:
- AmazonEC2ContainerServiceFullAccess
- AmazonEC2ContainerRegistryPowerUser

```shell
$ export POLICYARN=$(aws iam list-policies --query 'Policies[?PolicyName==`AmazonEC2ContainerServiceFullAccess`].{ARN:Arn}' --output text)
$ aws iam attach-user-policy --user-name $AWS_IAM_USER --policy-arn $POLICYARN
$ export POLICYARN=$(aws iam list-policies --query 'Policies[?PolicyName==`AmazonEC2ContainerRegistryPowerUser`].{ARN:Arn}' --output text)
$ aws iam attach-user-policy --user-name $AWS_IAM_USER --policy-arn $POLICYARN
```


Verify that the policy is attached to the user by running the `list-attached-user-policies` command.

```
$ aws iam list-attached-user-policies --user-name $AWS_IAM_USER
```

## AWS ECR

Environment variables
```shell
$ export AWS_DEFAULT_REGION=eu-west-1
$ export AWS_REPO_NAME=hello-nginx
$ export AWS_ACCOUNT_ID=0000000000000
$ export AWS_ECS_CLUSTER=up-cluster
$ export AWS_ECS_SERVICE=hello-nginx-service-prod
$ export AWS_ECS_TASK_DEFINITION=hello-nginx
```

Create a repository
```shell
aws ecr create-repository --repository-name $AWS_REPO_NAME --image-scanning-configuration scanOnPush=true
```

Get Amazon Resource Name (ARN) that identifies the repository (requires jq installed)
```shell
export AWS_REPO_ARN=$(aws ecr describe-repositories --repository-names $AWS_REPO_NAME | jq -r '.[] | .[] | .repositoryArn')
```

### Authenticate to Registry
Authenticate Docker to an Amazon ECR registry with get-login-password (valid for 12 hours)
```
aws ecr get-login-password --region $AWS_DEFAULT_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com
```

### Pushing an Image
```
docker build -t $AWS_REPO_NAME .
docker tag $AWS_REPO_NAME":latest" $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$AWS_REPO_NAME":latest"
docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$AWS_REPO_NAME":latest"
```

### Replace image
List ressources in terraform with `terraform state list`

Taint desired object and plan / apply changes
```
terraform taint module.task_def.aws_ecs_task_definition.nginx
terraform plan
terraform apply
```

### Pipeline: Replace image
- [UpdateService - Amazon Elastic Container Service](https://docs.aws.amazon.com/AmazonECS/latest/APIReference/API_UpdateService.html)
- [Updating a Service](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/update-service.html)
- [AWS ECS and ECR deployment via Docker and Gitlab CI](https://gist.github.com/jlis/4bc528041b9661ae6594c63cd2ef673c)

If your updated Docker image uses the same tag as what is in the existing task definition for your service (for example, `hello-nginx:latest`), you do not need to create a new revision of your task definition. You can update the service using the procedure below, keep the current settings for your service, and select Force new deployment.

```
aws ecs update-service --region $AWS_DEFAULT_REGION --cluster $AWS_ECS_CLUSTER --service $AWS_ECS_SERVICE --task-definition $AWS_ECS_TASK_DEFINITION --force-new-deployment
docker run -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY --rm -it amazon/aws-cli ecs update-service --region $AWS_DEFAULT_REGION --cluster $AWS_ECS_CLUSTER --service $AWS_ECS_SERVICE --task-definition $AWS_ECS_TASK_DEFINITION --force-new-deployment
```


## Gitlab CI/CD
In your GitLab project, go to Settings > CI / CD. Set the Access key ID and Secret access key as environment variables, using the following variable names:

Env. variable name
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

Debug docker, don't do it with dind ([Using Docker-in-Docker for your CI or testing environment? Think twice.](https://jpetazzo.github.io/2015/09/03/do-not-use-docker-in-docker-for-ci/))
```shell
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -e AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY docker
```
